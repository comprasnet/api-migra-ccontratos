<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contratohistorico extends Model
{
    protected $table = 'contratohistorico';
    protected $primaryKey = 'id';

    // ***** MUTATORS *****
    public function getTipoIdAttribute($value){
        $retorno = $this->formatarAtributoTipoId($value);
        return $retorno;
    }
    public function getFornecedorIdAttribute($value){
        $fornecedor = $this->buscaFornecedor($value);
        return $fornecedor;
    }
    public function getUnidadeIdAttribute($value){
        $unidade = $this->buscaUnidade($value);
        return $unidade;
    }
    public function getCategoriaIdAttribute($value){
        if($value==''){return null;}
        $categoria = $this->buscaCodigoitem($value);
        return $categoria;
    }

    // Métodos que auxiliam os mutators
    public function formatarAtributoTipoId($id){
        $retorno = Codigoitens::find($id);
        if($retorno){return $retorno->descricao;}
        else {return null;}
    }
    private function buscaFornecedor($id){
        $retorno = Fornecedores::find($id);
        return $retorno->cpf_cnpj_idgener . '|' . $retorno->nome;
    }
    private function buscaUnidade($id){
        $retorno = Unidades::find($id);
        return $retorno->codigo;
    }
    private function buscaCodigoitem($id){
        $retorno = Codigoitens::find($id);
        return $retorno->descricao;
    }
}
