<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ug extends Model
{
    protected $table = 'ug';

    protected $primaryKey = 'ug_ig';
}
