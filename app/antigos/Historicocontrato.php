<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historicocontrato extends Model
{
    protected $table = 'historicocontrato';

    protected $primaryKey = 'his_id';

    public function getHisTipoAttribute($value)
    {
        $descricao = $this->buscaCodigoItem($value);
        return $descricao;
    }

    public function getHisGarantiaAttribute($value)
    {
        $descricao = $this->buscaCodigoItem($value);
        return $descricao;
    }

    private function buscaCodigoItem($id)
    {
        $retorno = Codigo_item::find($id);
        return $retorno->cit_descricao;
    }

    public function contrato()
    {
        return $this->belongsTo(Contrato::class, 'his_con_id');
    }
}
