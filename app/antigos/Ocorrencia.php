<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ocorrencia extends Model
{
    protected $table = 'ocorrencia';

    protected $primaryKey = 'oco_id';



    public function getOcoFuncaoAttribute($value)
    {
        $descricao = $this->buscaFuncao($value);

        return $descricao;
    }

    private function buscaFuncao($id)
    {
        $retorno = Codigo_item::find($id);

        return $retorno->cit_descricao;
    }


    public function getOcoSituacaoAttribute($value)
    {
        $descricao = $this->buscaSituacao($value);

        return $descricao;
    }

    private function buscaSituacao($id)
    {
        $retorno = Codigo_item::find($id);

        return $retorno->cit_descricao;
    }

    public function getOcoStatusconclusaoAttribute($value)
    {
        if($value == 0){
            return '';
        }

        $descricao = $this->buscaStatusconclusao($value);

        return $descricao;
    }

    private function buscaStatusconclusao($id)
    {
        $retorno = Codigo_item::find($id);

        return $retorno->cit_descricao;
    }

    public function getOcoIdconclusaoAttribute($value)
    {
        if($value == 0){
            return '';
        }

        $descricao = $this->buscaIdconclusao($value);

        return $descricao;
    }

    private function buscaIdconclusao($id)
    {
        $retorno = $this->find($id);

        return $retorno->oco_num;
    }

    public function getOcoFiscalAttribute($value)
    {
        $fiscal = $this->buscaFiscal($value);

        return $fiscal;
    }

    private function buscaFiscal($id)
    {
        $url = new Contrato();
        $retorno = $url->montaUrlId('sec_users', $id);

        return $retorno;
    }
}
