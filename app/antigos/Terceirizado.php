<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terceirizado extends Model
{
    protected $table = 'terceirizado';

    protected $primaryKey = 'ter_id';


    public function getTerFuncaoAttribute($value)
    {
        $descricao = $this->buscaFuncao($value);

        return $descricao;
    }

    public function getTerEscolaridadeAttribute($value)
    {
        $descricao = $this->buscaEscolaridade($value);

        return $descricao;
    }

    private function buscaFuncao($id)
    {
        $retorno = Codigo_item::find($id);

        return $retorno->cit_descricao;
    }

    private function buscaEscolaridade($id)
    {
        $retorno = Codigo_item::find($id);

        return $retorno->cit_descricao;
    }


}
