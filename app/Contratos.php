<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contratos extends Model
{

    protected $table = 'contratos';
    protected $primaryKey = 'id';

    // chamado de ApiController
    public function buscarTodos(){
        $lista = [];
        $contratos = $this->where('deleted_at', null)->get();
        foreach ($contratos as $contrato) {
            $lista[] = $this->montaUrlId('contrato', $contrato->id);
        }
        return $lista;
    }
    // chamado de ApiController
    public function lista($id = null){
        $lista = [];
        $contratos = $this->where('deleted_at', null)->get();
        if ($id) {
            $contratos = $contratos->find($id);

            if (!$contratos) {
                return $lista;
            }
            $lista[0] = $contratos->toArray();
            $lista[0]['contratoresponsaveis'] = $this->buscaContratoresponsaveis($contratos);
            $lista[0]['contratocronograma'] = null; // informado pelo heles que não precisa
            $lista[0]['contratohistoricos'] = $this->buscaContratohistorico($contratos);
            $lista[0]['contrato_arquivos'] = $this->buscaContrato_arquivos($contratos);
            $lista[0]['contratodespesaacessoria'] = $this->buscaContratodespesaacessoria($contratos);
            $lista[0]['contratoempenhos'] = $this->buscaContratoempenhos($contratos);
            $lista[0]['contratofaturas'] = $this->buscaContratofaturas($contratos);
            $lista[0]['contratogarantias'] = $this->buscaContratogarantias($contratos);
            $lista[0]['contratoitens'] = $this->buscaContratoitens($contratos);
            $lista[0]['contratoocorrencias'] = $this->buscaContratoocorrencias($contratos);
            $lista[0]['contratopreposto'] = $this->buscaContratopreposto($contratos);
            $lista[0]['contratoterceirizados'] = $this->buscaContratoterceirizados($contratos);
            $lista[0]['importacoes'] = $this->buscaImportacoes($contratos);
            $lista[0]['subrogacoes'] = $this->buscaSubrogacoes($contratos);
            return $lista;
        }


        $t = 0;
        foreach ($contratos as $contrato) {
            $lista[0] = $contratos->toArray();
            $lista[0]['contratoresponsaveis'] = $this->buscaContratoresponsaveis($contratos);
            $lista[0]['contratocronograma'] = null; // informado pelo heles que não precisa
            $lista[0]['contratohistoricos'] = $this->buscaContratohistorico($contratos);
            $lista[0]['contrato_arquivos'] = $this->buscaContrato_arquivos($contratos);
            $lista[0]['contratodespesaacessoria'] = $this->buscaContratodespesaacessoria($contratos);
            $lista[0]['contratoempenhos'] = $this->buscaContratoempenhos($contratos);
            $lista[0]['contratofaturas'] = $this->buscaContratofaturas($contratos);
            $lista[0]['contratogarantias'] = $this->buscaContratogarantias($contratos);
            $lista[0]['contratoitens'] = $this->buscaContratoitens($contratos);
            $lista[0]['contratoocorrencias'] = $this->buscaContratoocorrencias($contratos);
            $lista[0]['contratopreposto'] = $this->buscaContratopreposto($contratos);
            $lista[0]['contratoterceirizados'] = $this->buscaContratoterceirizados($contratos);
            $lista[0]['importacoes'] = $this->buscaImportacoes($contratos);
            $lista[0]['subrogacoes'] = $this->buscaSubrogacoes($contratos);
            $t++;
        }
        return $lista;
    }
    public function montaUrlId($table = null, $id = null){
        return url('/api/v1') . '/' . $table . '/' . $id . '?token=' . config('app.key');
    }

    //
    private function buscaSubrogacoes(Contratos $contrato){
        $retorno = [];
        $subrogacoes = $contrato->subrogacoes()->orderBy('contrato_id','asc')->get();
        if( count($subrogacoes)>0 ){
            foreach ($subrogacoes as $contrato) {
                $retorno[] = $this->montaUrlId('subrogacoes', $contrato->id);
            }
            return $retorno;

        } else {
            return null;
        }
    }
    private function buscaImportacoes(Contratos $contrato){
        $retorno = [];
        $importacoes = $contrato->importacoes()->orderBy('contrato_id','asc')->get();
        if( count($importacoes)>0 ){
            foreach ($importacoes as $contrato) {
                $retorno[] = $this->montaUrlId('importacoes', $contrato->id);
            }
            return $retorno;

        } else {
            return null;
        }
    }
    private function buscaContratoterceirizados(Contratos $contrato){
        $retorno = [];
        $contratoterceirizados = $contrato->contratoterceirizados()->orderBy('contrato_id','asc')->get();
        if( count($contratoterceirizados)>0 ){
            foreach ($contratoterceirizados as $contrato) {
                $retorno[] = $this->montaUrlId('contratoterceirizados', $contrato->id);
            }
            return $retorno;

        } else {
            return null;
        }
    }
    private function buscaContratopreposto(Contratos $contrato){
        $retorno = [];
        $contratopreposto = $contrato->contratopreposto()->orderBy('contrato_id','asc')->get();
        if( count($contratopreposto)>0 ){
            foreach ($contratopreposto as $contrato) {
                $retorno[] = $this->montaUrlId('contratopreposto', $contrato->id);
            }
            return $retorno;

        } else {
            return null;
        }
    }
    private function buscaContratoocorrencias(Contratos $contrato){
        $retorno = [];
        $contratoocorrencias = $contrato->contratoocorrencias()->orderBy('contrato_id','asc')->get();
        if( count($contratoocorrencias)>0 ){
            foreach ($contratoocorrencias as $contrato) {
                $retorno[] = $this->montaUrlId('contratoocorrencias', $contrato->id);
            }
            return $retorno;

        } else {
            return null;
        }
    }
    private function buscaContratodespesaacessoria(Contratos $contrato){
        $retorno = [];
        $contratodespesaacessoria = $contrato->contratodespesaacessorias()->orderBy('contrato_id','asc')->get();
        if( count($contratodespesaacessoria)>0 ){
            foreach ($contratodespesaacessoria as $contrato) {
                $retorno[] = $this->montaUrlId('contratodespesaacessoria', $contrato->id);
            }
            return $retorno;

        } else {
            return null;
        }
    }
    private function buscaContratohistorico(Contratos $contrato){
        $retorno = [];
        $contratohistoricos = $contrato->contratohistoricos()->orderBy('contrato_id','asc')->get();
        if( count($contratohistoricos)>0 ){
            foreach ($contratohistoricos as $contrato) {
                $retorno[] = $this->montaUrlId('contratohistorico', $contrato->id);
            }
            return $retorno;
        } else {
            return null;
        }
    }
    private function buscaContratocronogramas(Contratos $contrato){
        $retorno = [];
        $contratocronogramas = $contrato->contratocronogramas()->orderBy('contrato_id','asc')->get();
        if( count($contratocronogramas)>0 ){
            foreach ($contratocronogramas as $contrato) {
                $retorno[] = $this->montaUrlId('contratocronograma', $contrato->id);
            }
            return $retorno;
        } else {
            return null;
        }
    }
    private function buscaContrato_arquivos(Contratos $contrato){
        $retorno = [];
        $contrato_arquivos = $contrato->contrato_arquivos()->orderBy('contrato_id','asc')->get();
        if( count($contrato_arquivos)>0 ){
            foreach ($contrato_arquivos as $contrato) {
                $retorno[] = $this->montaUrlId('contrato_arquivos', $contrato->id);
            }
            return $retorno;
        } else {
            return null;
        }
    }
    private function buscaContratofaturas(Contratos $contrato){
        $retorno = [];
        $contratofaturas = $contrato->contratofaturas()->orderBy('contrato_id','asc')->get();
        if( count($contratofaturas)>0 ){
            foreach ($contratofaturas as $contrato) {
                $retorno[] = $this->montaUrlId('contratofaturas', $contrato->id);
            }
            return $retorno;
        } else {
            return null;
        }
    }
    private function buscaContratoitens(Contratos $contrato){
        $retorno = [];
        $contratoitens = $contrato->contratoitens()->orderBy('contrato_id','asc')->get();
        if( count($contratoitens)>0 ){
            foreach ($contratoitens as $contrato) {
                $retorno[] = $this->montaUrlId('contratoitens', $contrato->id);
            }
            return $retorno;
        } else {
            return null;
        }
    }
    private function buscaContratogarantias(Contratos $contrato){
        $retorno = [];
        $contratogarantias = $contrato->contratogarantias()->orderBy('contrato_id','asc')->get();
        if( count($contratogarantias)>0 ){
            foreach ($contratogarantias as $contrato) {
                $retorno[] = $this->montaUrlId('contratogarantias', $contrato->id);
            }
            return $retorno;
        } else {
            return null;
        }
    }
    private function buscaContratoempenhos(Contratos $contrato){
        $retorno = [];
        $contratoempenhos = $contrato->contratoempenhos()->orderBy('contrato_id','asc')->get();
        if( count($contratoempenhos)>0 ){
            foreach ($contratoempenhos as $contrato) {
                $retorno[] = $this->montaUrlId('contratoempenhos', $contrato->id);
            }
            return $retorno;
        } else {
            return null;
        }
    }
    private function buscaContratoresponsaveis(Contratos $contrato){
        $retorno = [];
        $contratoresponsaveis = $contrato->contratoresponsaveis()->orderBy('contrato_id','asc')->get();
        if( count($contratoresponsaveis)>0 ){
            foreach ($contratoresponsaveis as $contrato) {
                $retorno[] = $this->montaUrlId('contratoresponsaveis', $contrato->id);
            }
            return $retorno;
        } else {
            return null;
        }
    }

    // tabelas que se relacionam com esta.
    public function subrogacoes(){return $this->hasMany(Subrogacoes::class, 'contrato_id');}
    public function importacoes(){return $this->hasMany(Importacoes::class, 'contrato_id');}
    public function contratoterceirizados(){return $this->hasMany(Contratoterceirizados::class, 'contrato_id');}
    public function contratopreposto(){return $this->hasMany(Contratopreposto::class, 'contrato_id');}
    public function contrato_arquivos(){return $this->hasMany(Contrato_arquivos::class, 'contrato_id');}
    public function contratoocorrencias(){return $this->hasMany(Contratoocorrencias::class, 'contrato_id');}
    public function contratofaturas(){return $this->hasMany(Contratofaturas::class, 'contrato_id');}
    public function contratogarantias(){return $this->hasMany(Contratogarantias::class, 'contrato_id');}
    public function contratoitens(){return $this->hasMany(Contratoitens::class, 'contrato_id');}
    public function contratoempenhos(){return $this->hasMany(Contratoempenhos::class, 'contrato_id');}
    public function contratohistoricos(){return $this->hasMany(Contratohistorico::class, 'contrato_id');}
    public function contratocronogramas(){return $this->hasMany(Contratocronograma::class, 'contrato_id');}
    public function contratoresponsaveis(){return $this->hasMany(Contratoresponsaveis::class, 'contrato_id');}
    public function contratodespesaacessorias(){return $this->hasMany(Contratodespesaacessoria::class, 'contrato_id');}
    private function buscaFornecedorByFk(Contratos $contrato){
        $retorno = [];
        $fornecedor = $contrato->getFornecedor($contrato->fornecedor_id);
        if($fornecedor){
            $retorno[] = $this->montaUrlId('fornecedores', $fornecedor->id);
            return $retorno;
        } else {return null;}
    }
    //
    // public function getSubcategoria($id){return Orgaosubcategorias::find($id);}
    public function getCodigoitens($id){
        return Codigoitens::find($id);
    }
    public function getFornecedor($id){
        return Fornecedores::find($id);
    }
    public function getUnidade($id){
        return Unidades::find($id);
    }



    // ***** MUTATORS *****
    public function getSubcategoriaIdAttribute($value){
        $retorno = $this->formatarAtributoSubcategoriaId($value);
        return $retorno;
    }
    public function getCategoriaIdAttribute($value){
        $retorno = $this->formatarAtributoCategoriaId($value);
        return $retorno;
    }
    public function getTipoIdAttribute($value){
        $retorno = $this->formatarAtributoTipoId($value);
        return $retorno;
    }
    public function getModalidadeIdAttribute($value){
        $retorno = $this->formatarAtributoModalidadeId($value);
        return $retorno;
    }
    public function getUnidadeIdAttribute($value){
        $retorno = $this->formatarAtributoUnidadeId($value);
        return $retorno;
    }
    public function getFornecedorIdAttribute($value){
        // retornar cpf_cnpj, nome e tipo (pessoa física ou jurídica)
        $retorno = $this->formatarAtributoFornecedorId($value);
        return $retorno;
    }
    public function getNumeroAttribute($value){
        // precisa ter 5 cadas antes da barra.
        $numero = $this->formatarAtributoNumero($value, 5);
        return $numero;
    }

    // Métodos que auxiliam os mutators
    public function formatarAtributoSubcategoriaId($id){
        $retorno = Orgaosubcategorias::find($id);
        if($retorno){return $retorno->descricao;}
        else {return null;}
    }
    public function formatarAtributoCategoriaId($id){
        $retorno = Codigoitens::find($id);
        if($retorno){return $retorno->descricao;}
        else {return null;}
    }
    public function formatarAtributoTipoId($id){
        $retorno = Codigoitens::find($id);
        if($retorno){return $retorno->descricao;}
        else {return null;}
    }
    public function formatarAtributoModalidadeId($id){
        $retorno = Codigoitens::find($id);
        if($retorno){return $retorno->descricao;}
        else {return null;}
    }
    public function formatarAtributoUnidadeId($id){
        $retorno = Unidades::find($id);
        if($retorno){return $retorno->codigo;}
        else {return null;}
    }
    public function formatarAtributoFornecedorId($id){
        $retorno = Fornecedores::find($id);
        if($retorno){return $retorno->cpf_cnpj_idgener . '|' . $retorno->nome . '|' . $retorno->tipo_fornecedor;}
        else {return null;}
    }
    public function formatarAtributoNumero($value, $quantidadeDeCasasAntesDaBarra){
        $posicaoBarra = strpos($value, '/');
        $valorAntesDaBarra = substr($value, 0, $posicaoBarra);
        $quantidadeCaracteres = strlen($valorAntesDaBarra);
        $quantidadeQueFalta = 0;
        if($quantidadeCaracteres < 5){$quantidadeQueFalta = ($quantidadeDeCasasAntesDaBarra - $quantidadeCaracteres);}
        while( $quantidadeQueFalta > 0 ){
            $value = ('0'.$value);
            $quantidadeQueFalta--;
        }
        return $value;
    }
}
