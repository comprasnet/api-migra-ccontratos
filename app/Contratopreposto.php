<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contratopreposto extends Model
{
    protected $table = 'contratopreposto';
    protected $primaryKey = 'id';
}
