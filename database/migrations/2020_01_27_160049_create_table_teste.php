<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTeste extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teste', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descricao');
            $table->string('tipo', 10)->nullable();
            $table->boolean('boleano');
            $table->char('texto_10', 10);
            $table->date('data');
            $table->dateTime('data_hora');
            $table->decimal('total', 15, 2);
            $table->integer('numero');
            $table->string('sha256');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teste');
    }
}
