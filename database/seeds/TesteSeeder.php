<?php

use Illuminate\Database\Seeder;

class TesteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Teste::class, 10)->create();
        factory(App\Teste::class, 5)->create([
            'tipo' => 'Tipo xyz'
        ]);
    }
}
