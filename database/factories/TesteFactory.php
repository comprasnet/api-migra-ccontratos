<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Teste;
use Faker\Generator as Faker;

$factory->define(Teste::class, function (Faker $faker) {
    return [
        'descricao' => $faker->realText(60),
        'tipo' => null,
        'boleano' => $faker->boolean,
        'texto_10' => $faker->text(10),
        'data' => $faker->date,
        'data_hora' => $faker->dateTimeThisYear,
        'total' => $faker->randomFloat,
        'numero' => $faker->randomNumber,
        'sha256' => $faker->sha256
    ];
});
