<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('conta:key:generate', function () {
    $this->call('key:generate');

    $APP_KEY = config('app.key');

    // Função especial para verificar se existe sinal de mais (+) no token
    while (strstr($APP_KEY, '/') || strstr($APP_KEY, '+')) {
        $this->error('Chave gerada com caracter especial... gerando nova chave');
        $this->call('key:generate');
        $APP_KEY = config('app.key');
    }

    $this->comment('Favor informar essa CHAVE para importar seus dados!');
    $this->comment(config('app.key'));
})->describe('Cria chave da aplicação para o Conta');

Artisan::command('conta:models:all', function () {
    $tables = [
        'activity_log',
        'app_version',
        'apropriacoes',
        'apropriacoes_fases',
        'apropriacoes_situacao',
        'calendarevents',
        'catmatsergrupos',
        'catmatseratualizacao',
        'apropriacoes_importacao',
        'centrocusto',
        'codigos',
        'contratoempenhos',
        'contrato_arquivos',
        'contratofaturas',
        'contratofatura_empenhos',
        'contratogarantias',
        'contratohistorico',
        'catmatseritens',
        'contratoitens',
        'contratoocorrencias',
        'contratocronograma',
        'instalacoes',
        'empenhodetalhado',
        'execsfsituacao',
        'contratoterceirizados',
        'failed_jobs',
        'fornecedores',
        'jobs',
        'empenhos',
        'documentosiafi',
        'migrations',
        'model_has_roles',
        'naturezadespesa',
        'planointerno',
        'orgaossuperiores',
        'notifications',
        'password_resets',
        'permissions',
        'orgaosubcategorias',
        'rhddp',
        'justificativafatura',
        'rhddpdetalhado',
        'rhsituacao',
        'sfdadosbasicos',
        'roles',
        'rhrubrica',
        'sfdeducao_encargo_dadospagto',
        'sfcertificado',
        'sfdespesaanularitem',
        'sfdespesaanular',
        'sfcentrocusto',
        'sfacrescimo',
        'sfitemrecolhimento',
        'sfoutroslanc',
        'sfnonce',
        'sfpco',
        'sfpredoc',
        'sfrelitemded',
        'sfpso',
        'sfrelitemdespanular',
        'sfpsoitem',
        'sfrelitemvlrcc',
        'sfdocorigem',
        'users',
        'apropriacoes_nota_empenho',
        'contratoresponsaveis',
        'model_has_permissions',
        'naturezasubitem',
        'tipolistafatura',
        'sfpcoitem',
        'contratopreposto',
        'sfpadrao',
        'unidadesusers',
        'rhsituacao_rhrubrica',
        'role_has_permissions',
        'sfdomiciliobancario',
        'unidadeconfiguracao',
        'saldohistoricoitens',
        'orgaoconfiguracao',
        'comunica',
        'orgaos',
        'migracaosistemaconta',
        'subrogacoes',
        'unidades',
        'importacoes',
        'codigoitens',
        'contratodespesaacessoria',
        'contratos',
        'activity_log_expurgo'
    ];

    foreach ($tables as $t) {
        $className = ucfirst($t);
        $this->comment('Criando Model para a tabela: ' . $className);
        $this->call('make:model', [
            'name' => $className
        ]);
        $this->comment('Atualizando nome da tabela no Model.');
        exec('sed -i "s%//%protected $table = \'' . $t . '\';%g" app/' . $className . '.php');
    }
})->describe('Cria os Models no Laravel a partir do Banco de Dados do Conta');
